#version 330 core

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

in vec3 in_Position;
in vec3 in_Position_Previous;
in float in_Alpha;
in vec2 texcoord;

out float out_alpha;
out vec2 Texcoord;

const vec2 pos[] = vec2[4](
                           vec2(-1,  1),
                           vec2(-1, -1),
                           vec2(1,   1),
                           vec2(1,  -1)
                           );

const vec2 tex[] = vec2[4](
                         vec2(0, 0),
                         vec2(0, 1),
                         vec2(1, 0),
                         vec2(1, 1)
                         );

const vec4 particleSize = vec4(0, 0.03, 0, 0);

void main(void)
{
    vec4 position = (projectionMatrix*viewMatrix)*vec4(in_Position.x,- in_Position.y + 0.54, in_Position.z, 1);
    vec4 prevPosition = (projectionMatrix*viewMatrix)*vec4(in_Position_Previous.x,- in_Position_Previous.y + 0.54, in_Position_Previous.z, 1);
    if (position.x > prevPosition.x) {
        switch (gl_VertexID) {
            case 0:
                gl_Position = prevPosition + particleSize;
                break;
            case 1:
                break;
                gl_Position = prevPosition;
            case 2:
                gl_Position = position + particleSize;
                break;
            case 3:
                gl_Position = position;
                break;
            default:
                break;
        }
    } else {
        switch (gl_VertexID) {
            case 0:
                gl_Position = position + particleSize;
                break;
            case 1:
                break;
                gl_Position = position;
            case 2:
                gl_Position = prevPosition + particleSize;
                break;
            case 3:
                gl_Position = prevPosition;
                break;
            default:
                break;
        }
    }
    out_alpha = in_Alpha;
    Texcoord = tex[gl_VertexID];
    //gl_Position = vec4(position.xy+offset, position.z, position.w);
    //gl_Position = projectionMatrix*(viewMatrix*vec4(in_Position.x, -in_Position.y, in_Position.z, 1)+ vec4(offset.x,offset.y+0.55,0,0));
    //gl_Position = (projectionMatrix*viewMatrix)*vec4(offset.x + in_Position.x, offset.y - in_Position.y + 0.54, in_Position.z, 1);
    
}