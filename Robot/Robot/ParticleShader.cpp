//
//  ParticleShader.cpp
//  Robot
//
//  Created by Adrian Krupa on 02.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "ParticleShader.h"
#include "Particle.h"


using namespace ParticleSystem;

std::string Shader::ParticleShader::getShaderName()
{
    return "ParticleShader";
}

void Shader::ParticleShader::bindAttributeLocations()
{
    
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribAngle, "in_Position_Previous");
    glBindAttribLocation(m_program, VertexAttribAlpha, "in_Alpha");
    
}

void Shader::ParticleShader::fetchUniformLocations()
{
    m_projectionMatrixUniform = glGetUniformLocation(m_program, "projectionMatrix");
    m_viewMatrixUniform = glGetUniformLocation(m_program, "viewMatrix");
    
}

GLuint Shader::ParticleShader::getProjectionMatrixUniform()
{
    return m_projectionMatrixUniform;
}

GLuint Shader::ParticleShader::getViewMatrixUniform()
{
    return m_viewMatrixUniform;
}