//
//  GeometryShader.h
//  Robot
//
//  Created by Adrian Krupa on 06.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__GeometryShader__
#define __Robot__GeometryShader__

#include <iostream>
#include <OpenGL/gl3.h>
#include "Shader.h"

namespace Shader {
    class GeometryShader : public Shader {
    public:
        virtual void setup();
    };
}

#endif /* defined(__Robot__GeometryShader__) */
