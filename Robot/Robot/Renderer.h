//
//  Renderer.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Renderer__
#define __Robot__Renderer__

#include <iostream>
#include <vector>
#include <OpenGL/gl3.h>

#include "RobotShader.h"
#include "ParticleShader.h"
#include "ShadowVolumeShader.h"
#include "ArmMeshBuffer.h"
#include "Model.h"
#include "CameraController.h"
#include "ParticleSystem.h"
#include "glm/glm.hpp"

namespace Renderer {
    class Renderer {
    
        Shader::RobotShader *robotShader;
        Shader::ShadowVolumeShader *shadowVolumeShader;
        
        std::vector<Buffer::ArmMeshBuffer *> m_armMeshBuffers;
        std::vector<Buffer::ArmMeshBuffer *> m_armMeshAdjBuffers;
        std::vector<Model::Model *> m_robotModels;

        std::vector<Buffer::ArmMeshBuffer *> m_sceneMeshBuffers;
        std::vector<Model::Model *> m_sceneModels;
        
        Buffer::ArmMeshBuffer *m_mirrorBuffer;
        Model::Model *m_mirrorModel;
        
        glm::vec3 m_lightPosition;
        
        ParticleSystem::ParticleSystem *m_particleSystem;
        
        void drawMirror(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
        void drawScene(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
        void drawAmbient();
    public:
        void setup();
        
        void setParticleSystem(ParticleSystem::ParticleSystem &particleSystem);
        void setRobotModels(std::vector<Model::Model *> &robotModels);
        void setSceneModels(std::vector<Model::Model *> *sceneModels);
        
        void setMirrorModel(Model::Model *mirrorModel);
        
        void renderWithCameraController(Controller::CameraController *controller);
    private:
        void renderSceneIntoDepthBufferWithCameraController(Controller::CameraController *controller);
        void renderShadowsVolumesIntoStencilBufferWithCameraController(Controller::CameraController *controller);
        void renderShadowedSceneWithCameraController(Controller::CameraController *controller);
    };
}

#endif /* defined(__Robot__Renderer__) */
