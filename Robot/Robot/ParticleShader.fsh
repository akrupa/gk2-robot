#version 330 core

in float out_alpha;
in vec2 Texcoord;

out vec4 out_Color;

uniform sampler2D tex;

void main(void) {
    
    vec4 color = texture(tex, Texcoord);
    color.w = (color.x + color.y + color.z)/3;
	out_Color = color * vec4(1,1,1,out_alpha);
}