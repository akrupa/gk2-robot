//
//  ParticleSystem.cpp
//  Robot
//
//  Created by Adrian Krupa on 02.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "ParticleSystem.h"
#include "glm/gtc/type_ptr.hpp"
#include "SOIL/SOIL.h"

namespace ParticleSystem {
    
    ParticleSystem::ParticleSystem() {

    }
    
    ParticleSystem::~ParticleSystem() {
        
    }
    
    
    void ParticleSystem::setup(vec3 planeNormal) {
        
        srandom(0);
        
        m_planeNormal = planeNormal;
        bytes_allocated = 0;
        
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        
        
        glEnableVertexAttribArray(VertexAttribPosition); // pos
        glEnableVertexAttribArray(VertexAttribAngle); // vel
        glEnableVertexAttribArray(VertexAttribAlpha); // alpha
        
        
        glVertexAttribPointer(VertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*) offsetof(Particle, position)); //in_position
        glVertexAttribPointer(VertexAttribAngle, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*) offsetof(Particle, previous_position)); //in_velocity
        glVertexAttribPointer(VertexAttribAlpha, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*) offsetof(Particle, alpha)); //in_alpha
        
        glVertexAttribDivisor(VertexAttribPosition, 1);
        glVertexAttribDivisor(VertexAttribAngle, 1);
        glVertexAttribDivisor(VertexAttribAlpha, 1);
        
        
        
        
        particleShader = new Shader::ParticleShader();
        particleShader->setup();
        
        GLint texAttrib = glGetAttribLocation(particleShader->getProgram(), "texcoord");
        glEnableVertexAttribArray(texAttrib);
        glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE,sizeof(Particle), (GLvoid*) offsetof(Particle, texCoords));
        
        glBindVertexArray(0);
        
        int img_width, img_height;
        unsigned char* img = SOIL_load_image("fire4.png", &img_width, &img_height, NULL, 0);
        
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img_width, img_height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
        SOIL_free_image_data(img);
        

    }
    
    void ParticleSystem::update(vec3 particleSystemPosition, float dt) {
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        
        for (std::vector<Particle>::iterator it = particles.begin() ; it != particles.end();) {
            it -> velocity.y += 9.81*dt;
            it -> previous_position = it -> position;
            it -> position += it -> velocity * dt;
            it -> life += dt;
            it -> alpha = 1.0 * (1.0 - it->life/MAX_PARTICLE_LIFETIME);
            if (it -> life > MAX_PARTICLE_LIFETIME) {
                it = particles.erase(it);
            } else {
                it++;
            }
        }
        
    
        if (particles.size() < MAX_PARTICLES) {
            for (int i=0; i<MAX_PARTICLES_PER_FRAME; i++) {
                if (particles.size() >= MAX_PARTICLES) {
                    break;
                }
                
                vec3 velocity = 2.0f * m_planeNormal + vec3(RandomFloat(-1.0, 1.0), RandomFloat(-1.0, 1.0), RandomFloat(-1.0, 1.0));
                velocity = normalize(velocity) * RandomFloat(MIN_PARTICLE_VELOCITY, MAX_PARTICLE_VELOCITY);
                
                
                Particle particle;
                particle.position = particleSystemPosition;
                particle.previous_position = particle.position;
                particle.velocity = velocity;
                particle.position += particle.velocity*dt;
                particle.life = 0;
                particle.alpha = 1;
                particles.push_back(particle);
            }
        }

                
        size_t bytes_needed = sizeof(Particle) * particles.size();
        if(bytes_needed > bytes_allocated) {
            glBufferData(GL_ARRAY_BUFFER, bytes_needed, &particles[0].position, GL_DYNAMIC_DRAW);
            bytes_allocated = bytes_needed;
        }
        else {
            glBufferSubData(GL_ARRAY_BUFFER, 0, bytes_needed, &particles[0].position);
        }
        
    }
    
    void ParticleSystem::drawWithCameraController(glm::mat4 viewMatrix, glm::mat4 projectionMatrix) {
        
        glDisable(GL_DEPTH);
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        
        glBindVertexArray(vao);
        glUseProgram(particleShader->getProgram());
        
        glUniformMatrix4fv(particleShader->getProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix));
        glUniformMatrix4fv(particleShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
        
        glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, (int)particles.size());
        
        glUseProgram(0);
        glBindVertexArray(0);
        
        glDisable(GL_BLEND);
        glDepthMask(GL_TRUE);
    }
    
    float ParticleSystem::RandomFloat(float a, float b) {
        float rndm = ((float) random()) / (float) RAND_MAX;
        float diff = b - a;
        float r = rndm * diff;
        return a + r;
    }
}
