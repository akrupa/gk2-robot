#version 330 core

uniform mat4 viewProjectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform vec4 drawColor;

uniform vec4 lightPosition;

in vec3 in_Position;
in vec3 in_Normal;



out vec4 pass_Color;


void main(void)
{
	vec4 normal = modelMatrix * vec4(in_Normal, 0.0);
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);

    vec4 toLight = normalize(lightPosition - worldPosition);

	float intensity = mix(1.0, dot(normal, toLight), 0.4);
    
	pass_Color = drawColor * vec4(intensity, intensity, intensity, 1.0);
  
    
	gl_Position = viewProjectionMatrix * worldPosition;
}