//
//  ParticleShader.h
//  Robot
//
//  Created by Adrian Krupa on 02.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__ParticleShader__
#define __Robot__ParticleShader__

#include <iostream>
#include "Shader.h"

namespace Shader {
    class ParticleShader : public Shader {
        
    private:
        GLuint m_projectionMatrixUniform;
        GLuint m_viewMatrixUniform;
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getProjectionMatrixUniform();
        GLuint getViewMatrixUniform();
        
    };
}

#endif /* defined(__Robot__ParticleShader__) */
