//
//  Particle.h
//  Robot
//
//  Created by Adrian Krupa on 02.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Particle__
#define __Robot__Particle__

#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

namespace ParticleSystem {
    class Particle {
        
    public:
        Particle();
        ~Particle();
        
    public:
        vec3 position;
        vec3 previous_position;
        vec3 velocity;
        float alpha;
        float life;
        vec2 texCoords;
    };
}

#endif /* defined(__Robot__Particle__) */
