//
//  ShadowVolumeShader.h
//  Robot
//
//  Created by Adrian Krupa on 06.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__ShadowVolumeShader__
#define __Robot__ShadowVolumeShader__

#include <iostream>
#include "GeometryShader.h"

namespace Shader {
    class ShadowVolumeShader : public GeometryShader {
        
    private:
        GLuint m_viewProjectionMatrixUniform;
        GLuint m_viewMatrixUniform;
        GLuint m_modelMatrixUniform;
        
        GLuint m_lightPositionUniform;
        GLuint m_viewProjectionGeometryMatrixUniform;

        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getViewProjectionMatrixUniform();
        GLuint getViewMatrixUniform();
        GLuint getModelMatrixUniform();

        GLuint getLightPositionUniform();
        GLuint getViewProjectionGeometryMatrixUniform();
    };
}

#endif /* defined(__Robot__ShadowVolumeShader__) */
