//
//  Buffer.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Buffer.h"


GLuint Buffer::Buffer::getVAO()
{
    return m_VAO;
}

GLuint Buffer::Buffer::getIndicesCount()
{
    return m_indiciesCount;
}