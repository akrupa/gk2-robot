//
//  GeometryShader.cpp
//  Robot
//
//  Created by Adrian Krupa on 06.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "GeometryShader.h"
#include <string>
#include <fstream>
#include <streambuf>

void Shader::GeometryShader::setup()
{
    GLuint vertShader, fragShader, geoShader;
    std::string vertShaderPathname, fragShaderPathname, geoShaderPathname;
    
    
    m_program = glCreateProgram();
    
    vertShaderPathname = getShaderName() + ".vsh";
    if (!compileShader(&vertShader, GL_VERTEX_SHADER, vertShaderPathname)) {
        std::cout << "Failed to compile vertex shader: " + getShaderName();
        return;
    }
    
    fragShaderPathname = getShaderName() + ".fsh";
    if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fragShaderPathname)) {
        std::cout << "Failed to compile fragment shader: " + getShaderName();
        return;
    }
    
    geoShaderPathname = getShaderName() + ".gsh";
    if (!compileShader(&geoShader, GL_GEOMETRY_SHADER, geoShaderPathname)) {
        std::cout << "Failed to compile geometry shader: " + getShaderName();
        return;
    }
    
    
    glAttachShader(m_program, vertShader);
    glAttachShader(m_program, fragShader);
    glAttachShader(m_program, geoShader);
    
    bindAttributeLocations();
    
    
    if (!linkProgram(m_program)) {
        std::cout << "Failed to link program: " + getShaderName();
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (geoShader) {
            glDeleteShader(geoShader);
            geoShader = 0;
        }
        if (m_program) {
            glDeleteProgram(m_program);
            m_program = 0;
        }
    }
    
    fetchUniformLocations();
    
    
    if (vertShader) {
        glDetachShader(m_program, vertShader);
        glDeleteShader(vertShader);
    }
    
    if (fragShader) {
        glDetachShader(m_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    if (geoShader) {
        glDetachShader(m_program, geoShader);
        glDeleteShader(geoShader);
    }
}