//
//  RobotController.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__RobotController__
#define __Robot__RobotController__

#include <iostream>
#include <vector>

#include "Model.h"

namespace Controller {
    class RobotController {
        
        float m_alpha;
        
        glm::vec3 m_planePosition;
        glm::vec3 m_planeNormal;

        glm::vec3 m_tipPosition;
        
        std::vector<Model::Model *> m_arms;
    public:
        RobotController();
        
        void simulate(float dt);
        
        std::vector<Model::Model *>& getArms();
        void setPlanePosition(glm::vec3 position);
        void setPlaneNormal(glm::vec3 normal);
        
        glm::vec3 getTipPosition();
    };
}
#endif /* defined(__Robot__RobotController__) */
