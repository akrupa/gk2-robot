//
//  Renderer.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Renderer.h"
#include "RobotShader.h"
#include "Mesh.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"


using namespace glm;

void Renderer::Renderer::setup()
{
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClearStencil(0);
    //glEnable(GL_DEPTH_CLAMP);
    m_lightPosition = vec3(-4.0f, 3.0f, 3.0f);
    
    robotShader = new Shader::RobotShader();
    robotShader->setup();
    
    shadowVolumeShader = new Shader::ShadowVolumeShader();
    shadowVolumeShader->setup();
    
    m_armMeshBuffers = std::vector<Buffer::ArmMeshBuffer *>();
    m_armMeshAdjBuffers = std::vector<Buffer::ArmMeshBuffer *>();
}

void Renderer::Renderer::setRobotModels(std::vector<Model::Model *> &robotModels)
{
    m_robotModels = robotModels;
    
    
    for (auto arm : robotModels) {
        auto mesh = arm->getMesh();
        
        Buffer::ArmMeshBuffer *buffer = new Buffer::ArmMeshBuffer();
        buffer->setup();
        
        buffer->fillVertexData(mesh->vertexes.data(),
                               mesh->vertexes.size() * sizeof(Vertex),
                               0);
        
        buffer->fillIndexData((unsigned int *)mesh->triangles.data(),
                              mesh->triangles.size() * sizeof(Triangle),
                              0);
        
        m_armMeshBuffers.push_back(buffer);
        
        Buffer::ArmMeshBuffer *adjBuffer = new Buffer::ArmMeshBuffer();
        adjBuffer->setup();
        
        adjBuffer->fillVertexData(mesh->positions.data(),
                                  mesh->positions.size() * sizeof(Vertex),
                                  0);
        
        adjBuffer->fillIndexData((unsigned int *)mesh->adjs.data(),
                                 mesh->adjs.size() * sizeof(Adj),
                                 0);
        
        m_armMeshAdjBuffers.push_back(adjBuffer);
    }
}

void Renderer::Renderer::setSceneModels(std::vector<Model::Model *> *sceneModels)
{
    m_sceneModels = *sceneModels;
    
    
    for (auto model : m_sceneModels) {
        auto mesh = model->getMesh();
        
        Buffer::ArmMeshBuffer *buffer = new Buffer::ArmMeshBuffer();
        buffer->setup();
        
        buffer->fillVertexData(mesh->vertexes.data(),
                               mesh->vertexes.size() * sizeof(Vertex),
                               0);
        
        buffer->fillIndexData((unsigned int *)mesh->triangles.data(),
                              mesh->triangles.size() * sizeof(Triangle),
                              0);
        
        m_sceneMeshBuffers.push_back(buffer);
    }
}

void Renderer::Renderer::setMirrorModel(Model::Model *mirrorModel)
{
    m_mirrorModel = mirrorModel;
    
    m_mirrorBuffer = new Buffer::ArmMeshBuffer();
    m_mirrorBuffer->setup();
    
    m_mirrorBuffer->fillVertexData(mirrorModel->getMesh()->vertexes.data(),
                                   mirrorModel->getMesh()->vertexes.size() * sizeof(Vertex),
                                   0);
    
    m_mirrorBuffer->fillIndexData((unsigned int *)mirrorModel->getMesh()->triangles.data(),
                                  mirrorModel->getMesh()->triangles.size() * sizeof(Triangle),
                                  0);
}

void Renderer::Renderer::setParticleSystem(ParticleSystem::ParticleSystem &particleSystem)
{
    m_particleSystem = &particleSystem;
}

void Renderer::Renderer::renderWithCameraController(Controller::CameraController *camera)
{
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    
    glEnable(GL_CULL_FACE);
    
    glCullFace(GL_BACK);
    
    
    // preparing stencil
    
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    glDepthMask(GL_FALSE);
    glEnable(GL_STENCIL_TEST);
    
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    
    drawMirror(camera->getViewMatrix(), camera->getProjectionMatrix());
    
    // mirrored scene drawing
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilFunc(GL_EQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    
    mat4 modelMatrix = m_mirrorModel->getTransformMatrix();
    
    glm::mat4 mirrorViewMatrix = camera->getViewMatrix() * modelMatrix * glm::scale(mat4(), vec3(1.0f, 1.0f, -1.0f)) * glm::inverse(modelMatrix);
    
    glm::mat4 mirrorProjectionMatrix = camera->getProjectionMatrix();
    
    glCullFace(GL_FRONT);
    
    drawScene(mirrorViewMatrix, mirrorProjectionMatrix);
    
    glDisable(GL_STENCIL_TEST);
    glCullFace(GL_BACK);
    glClear(GL_STENCIL_BUFFER_BIT);
    
    
    // rendering mirror surface to depth buffer
    
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    
    drawMirror(camera->getViewMatrix(), camera->getProjectionMatrix());
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    
    
    // drawing regular scene
    
    drawScene(camera->getViewMatrix(), camera->getProjectionMatrix());
    
    
    
    // final semi transparent mirror drawing
    
    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LEQUAL);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    
    drawMirror(camera->getViewMatrix(), camera->getProjectionMatrix());
    
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
    
    
    
    // shadows
    
    
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    glDepthMask(GL_FALSE);
    
    glDisable(GL_CULL_FACE);
    
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_ALWAYS, 0, 0xff);
    
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_INCR_WRAP);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_KEEP, GL_DECR_WRAP);
    
    renderShadowsVolumesIntoStencilBufferWithCameraController(camera);
    
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilFunc(GL_NOTEQUAL, 0x0, 0xFF);
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_FALSE);
    
    drawAmbient();
    glDisable(GL_STENCIL_TEST);

    //glClear(GL_COLOR_BUFFER_BIT);
    
    glDepthMask(GL_TRUE);

    glEnable(GL_CULL_FACE);

    
    
    //renderShadowedSceneWithCameraController(camera);
    
    //glDisable(GL_STENCIL_TEST);
    
    //glDrawBuffer(GL_FRONT);
    
    
    glFlush();
}

void Renderer::Renderer::drawMirror(glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
{
    glUseProgram(robotShader->getProgram());
    
    glUniformMatrix4fv(robotShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform4fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    {
        glBindVertexArray(m_mirrorBuffer->getVAO());
        mat4 modelMatrix = m_mirrorModel->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_mirrorModel->getMesh()->triangles.size() * 3);
        glUniform4fv(robotShader->getDrawColorUniform(), 1, glm::value_ptr(m_mirrorModel->color));
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::Renderer::drawScene(glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
{
    glUseProgram(robotShader->getProgram());
    
    glUniformMatrix4fv(robotShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform4fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    
    
    for (int i = 0; i < m_robotModels.size(); i++) {
        glBindVertexArray(m_armMeshBuffers[i]->getVAO());
        
        mat4 modelMatrix = m_robotModels[i]->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_robotModels[i]->getMesh()->triangles.size() * 3);
        glUniform4fv(robotShader->getDrawColorUniform(), 1, glm::value_ptr(m_robotModels[i]->color));
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    
    for (int i = 0; i < m_sceneModels.size(); i++) {
        glBindVertexArray(m_sceneMeshBuffers[i]->getVAO());
        
        mat4 modelMatrix = m_sceneModels[i]->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_sceneModels[i]->getMesh()->triangles.size() * 3);
        glUniform4fv(robotShader->getDrawColorUniform(), 1, glm::value_ptr(m_sceneModels[i]->color));
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    
    
    glUseProgram(0);
    glBindVertexArray(0);
    
    glDisable(GL_CULL_FACE);
    m_particleSystem->drawWithCameraController(viewMatrix, projectionMatrix);
    glEnable(GL_CULL_FACE);
    
    
    //glutSwapBuffers();
    
}

void Renderer::Renderer::renderSceneIntoDepthBufferWithCameraController(Controller::CameraController *camera)
{
    //glDrawBuffer(GL_NONE);
    glDepthMask(GL_TRUE);
    
    //TODO: null program
    glUseProgram(robotShader->getProgram());
    
    mat4 viewMatrix = camera->getViewMatrix();
    mat4 projectionMatrix = camera->getProjectionMatrix();
    
    glUniformMatrix4fv(robotShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform4fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    for (int i = 0; i < m_robotModels.size(); i++) {
        glBindVertexArray(m_armMeshBuffers[i]->getVAO());
        
        mat4 modelMatrix = m_robotModels[i]->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_robotModels[i]->getMesh()->triangles.size() * 6);
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
}

void Renderer::Renderer::renderShadowsVolumesIntoStencilBufferWithCameraController(Controller::CameraController *camera)
{

    //TODO: program with geometry shader
    glUseProgram(shadowVolumeShader->getProgram());
    
    mat4 viewMatrix =  camera->getViewMatrix();
    mat4 projectionMatrix = camera->getProjectionMatrix();
    
    glUniformMatrix4fv(shadowVolumeShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(shadowVolumeShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform3fv(shadowVolumeShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    glUniformMatrix4fv(shadowVolumeShader->getViewProjectionGeometryMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    for (int i = 0; i < m_robotModels.size(); i++) {
        glBindVertexArray(m_armMeshAdjBuffers[i]->getVAO());
        
        mat4 modelMatrix = m_robotModels[i]->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_robotModels[i]->getMesh()->adjs.size() * 6);
        
        glUniformMatrix4fv(shadowVolumeShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES_ADJACENCY, indexCount, GL_UNSIGNED_INT, 0);
    }
    
}

void Renderer::Renderer::renderShadowedSceneWithCameraController(Controller::CameraController *camera)
{
    glDrawBuffer(GL_BACK);
    
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilFunc(GL_EQUAL, 0x0, 0xFF);
    
    glUseProgram(robotShader->getProgram());
    
    mat4 viewMatrix = camera->getViewMatrix();
    mat4 projectionMatrix = camera->getProjectionMatrix();
    
    glUniformMatrix4fv(robotShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform4fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    for (int i = 0; i < m_robotModels.size(); i++) {
        glBindVertexArray(m_armMeshBuffers[i]->getVAO());
        
        mat4 modelMatrix = m_robotModels[i]->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_robotModels[i]->getMesh()->triangles.size() * 3);
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
}


void Renderer::Renderer::drawAmbient()
{
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    
    glUseProgram(robotShader->getProgram());
    
    glUniformMatrix4fv(robotShader->getViewMatrixUniform(), 1, 0, glm::value_ptr(mat4()));
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(mat4()));
    
    glUniform4fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    {
        glBindVertexArray(m_mirrorBuffer->getVAO());
        
        GLuint indexCount = (GLuint)(m_mirrorModel->getMesh()->triangles.size() * 3);
        glUniform4fv(robotShader->getDrawColorUniform(), 1, glm::value_ptr(vec4(0.0,0.0,0.0,0.2)));
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(mat4()));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    
    glUseProgram(0);
    glBindVertexArray(0);
    
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
}