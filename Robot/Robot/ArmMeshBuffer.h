//
//  ArmMeshBuffer.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__ArmMeshBuffer__
#define __Robot__ArmMeshBuffer__

#include <iostream>
#include "Buffer.h"
#include "Vertex.h"

namespace Buffer {
    class ArmMeshBuffer : public Buffer {
        
        
    public:
        void setup();
        void fillVertexData(Vertex *data, size_t size, size_t offset);
        void fillIndexData(GLuint *data, size_t size, size_t offset);
    };
}

#endif /* defined(__Robot__ArmMeshBuffer__) */
