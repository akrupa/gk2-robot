//
//  CameraController.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "CameraController.h"

#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

namespace Controller {
    
    void CameraController::mouseDrag(int x, int y)
    {
        const float Scale = 0.01;
        
        if (fabs(m_longitude) != (float)M_PI_2) {
            m_latitude += x * Scale;
        }
        m_longitude += y * Scale;
        
        m_longitude = clamp<float>(m_longitude, -M_PI_2, M_PI_2);
    }
    
    void CameraController::keyboardMove(float x, float y, float z)
    {
        const float Scale = 0.04f;
        
        vec3 diff = Scale * vec3(x, y, z);

        mat4 latRotation = glm::rotate(mat4(), -m_latitude, vec3(0.0f, 1.0f, 0.0f));
        mat4 longRotation = glm::rotate(mat4(), -m_longitude, vec3(1.0f, 0.0f, 0.0f));
        
        m_position += vec3(latRotation * longRotation * vec4(diff, 0.0f));
    }

    
    vec3 CameraController::getPosition()
    {
        return m_position;
    }

    mat4 CameraController::getViewMatrix()
    {

        mat4 translation = translate(mat4(), m_position);
        mat4 latRotation = glm::rotate(mat4(), m_latitude, vec3(0.0f, 1.0f, 0.0f));
        mat4 longRotation = glm::rotate(mat4(), m_longitude, vec3(1.0f, 0.0f, 0.0f));

        
        return longRotation * latRotation * translation;
    }
    
    mat4 CameraController::getProjectionMatrix()
    {
        mat4 perspective = glm::perspective((float)M_PI_4, 4.0f/3.0f, 0.2f, 1000.0f);
        
        return perspective;
    }
}