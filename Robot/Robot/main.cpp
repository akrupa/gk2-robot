//
//  main.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>
//#include <OpenGL/OpenGL.h>

#include "CameraController.h"
#include "RobotController.h"
#include "ParticleSystem.h"
#include "Renderer.h"
#include "PlaneMesh.h"
#include "CylinderMesh.h"

static const glm::vec3 MirrorPosition = vec3(-1.5f, 0.0f, 0.0f);
static const float MirrorTiltAngle = M_PI_2/3;

static const glm::vec3 CylinderPosition = vec3(2.0f, -0.75f, -1.5f);

static Controller::CameraController cameraController;
static Controller::RobotController robotController;
static ParticleSystem::ParticleSystem particleSystem;
static Renderer::Renderer renderer;



// ugly, but whatever...
static int mousePrevX;
static int mousePrevY;


static int keyboardX;
static int keyboardY;
static int keyboardZ;

void moveHandler(int x, int y)
{
    mousePrevX = x;
    mousePrevY = y;
}

void dragHandler(int x, int y)
{
    int dx = x - mousePrevX;
    int dy = y - mousePrevY;
    
    cameraController.mouseDrag(dx, dy);
    
    mousePrevX = x;
    mousePrevY = y;
}


void keyboardHandler(unsigned char key, int x, int y)
{
    if (key == 'a') {
        keyboardX = +1;
    }
    if (key == 'd') {
        keyboardX = -1;
    }
    if (key == 'w') {
        keyboardZ = +1;
    }
    if (key == 's') {
        keyboardZ = -1;
    }
    if (key == 'q') {
        keyboardY = +1;
    }
    if (key == 'e') {
        keyboardY = -1;
    }
}

void keyboardUpHandler(unsigned char key, int x, int y)
{
    keyboardX = 0;
    keyboardY = 0;
    keyboardZ = 0;
}



void display(void)
{
    cameraController.keyboardMove(keyboardX, keyboardY, keyboardZ);
    
    renderer.renderWithCameraController(&cameraController);
    robotController.simulate(1.0/60.0);
    particleSystem.update(robotController.getTipPosition(), 1.0/60.0);
    
    glutPostRedisplay();
}


Model::Model * mirrorMeshModel()
{
    PlaneMesh *mirrorMesh = new PlaneMesh();
    Model::Model * mirrorModel = new Model::Model(mirrorMesh);
    
    mat4 rotation = glm::rotate(mat4(), (float)-M_PI_2, vec3(0.0f, 1.0f, 0.0f));
    mat4 tilt = glm::rotate(mat4(), MirrorTiltAngle, vec3(0.0f, 0.0f, 1.0f));
    mat4 translation = translate(mat4(), MirrorPosition);
    
    mirrorModel->setTransformMatrix(translation * tilt * rotation);
    mirrorModel->color = vec4(0.5f, 0.5f, 0.5f, 0.5);
    
    return mirrorModel;
}


std::vector<Model::Model *> *sceneModels()
{
    auto models = new std::vector<Model::Model *>;
    
    PlaneMesh *wallMesh = new PlaneMesh();
    
    float scale = 6.0f;
    
    float upTr = + scale - 1.0f;
    
    mat4 rotations[6] = {
        glm::rotate(mat4(), (float)(0 * M_PI_2), vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(mat4(), (float)(1 * M_PI_2), vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(mat4(), (float)(2 * M_PI_2), vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(mat4(), (float)(3 * M_PI_2), vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::rotate(mat4(), (float)(-M_PI_2), vec3(0.0f, 1.0f, 0.0f)), (float)-M_PI_2, vec3(0, 0, 1)),
        glm::rotate(glm::rotate(mat4(), (float)( M_PI_2), vec3(0.0f, 1.0f, 0.0f)), (float)-M_PI_2, vec3(0, 0, -1)),
    };
    mat4 translations[6] = {
        glm::translate(mat4(), vec3(0.0f, +upTr, scale)),
        glm::translate(mat4(), vec3(0.0f, -scale + upTr, 0.0f)),
        glm::translate(mat4(), vec3(0.0f, upTr, -scale)),
        glm::translate(mat4(), vec3(0.0f, scale + upTr, 0.0f)),
        glm::translate(mat4(), vec3(-scale, upTr, 0.0f)),
        glm::translate(mat4(), vec3( scale, upTr, 0.0f)),
    };
    
    vec4 colors[6] = {
        vec4(0.3f, 0.0f, 0.0f, 1.0f),
        vec4(0.0f, 0.3f, 0.0f, 1.0f),
        vec4(0.3f, 0.0f, 0.0f, 1.0f),
        vec4(0.0f, 0.0f, 0.3f, 1.0f),
        vec4(0.3f, 0.0f, 0.0f, 1.0f),
        vec4(0.3f, 0.0f, 0.0f, 1.0f),
    };
    
    for (int i = 0; i < 6; i++) {
        Model::Model * wall = new Model::Model(wallMesh);
        
        wall->setTransformMatrix(translations[i] * rotations[i] * glm::scale(mat4(), vec3(scale, scale, -scale)));
        wall->color = colors[i];
        models->push_back(wall);
    }

    Model::Model * cylinder = new Model::Model(new CylinderMesh());
    
    cylinder->setTransformMatrix(glm::translate(mat4(), CylinderPosition) * glm::rotate(mat4(), (float)M_PI_4, vec3(0, 1, 0)));
    cylinder->color = vec4(0.7, 0.7, 0.0, 1.0);
    models->push_back(cylinder);
    
    return models;
}


void setup()
{
    renderer.setup();
    renderer.setRobotModels(robotController.getArms());
    renderer.setSceneModels(sceneModels());
    renderer.setMirrorModel(mirrorMeshModel());
    
    vec3 planeNormal = glm::vec3(cos(MirrorTiltAngle), -sin(MirrorTiltAngle), 0.0f);
    
    particleSystem.setup(planeNormal);
    
    robotController.setPlanePosition(MirrorPosition);
    robotController.setPlaneNormal(planeNormal);
    
    renderer.setParticleSystem(particleSystem);
}

int main (int argc, char **argv) {
    glutInit(&argc,argv);
    
#ifdef __APPLE__
	glutInitDisplayMode( GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_STENCIL);
#else
	// If you are using freeglut, the next two lines will check if
	// the code is truly 3.2. Otherwise, comment them out
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif
    
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Robot");
    
    glutDisplayFunc(display);
    glutPassiveMotionFunc(moveHandler);
    glutMotionFunc(dragHandler);
    glutKeyboardFunc(keyboardHandler);
    glutKeyboardUpFunc(keyboardUpHandler);
    
    setup();
    
    glutMainLoop();
    
    return 0;
}

