//
//  RobotShader.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "RobotShader.h"



std::string Shader::RobotShader::getShaderName()
{
    return "RobotShader";
}

void Shader::RobotShader::bindAttributeLocations()
{
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    
}

void Shader::RobotShader::fetchUniformLocations()
{
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_viewMatrixUniform = glGetUniformLocation(m_program, "viewMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    
    m_lightPositionUniform = glGetUniformLocation(m_program, "lightPosition");
    m_drawColorUniform = glGetUniformLocation(m_program, "drawColor");
    
}



GLuint Shader::RobotShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::RobotShader::getDrawColorUniform()
{
    return m_drawColorUniform;
}

GLuint Shader::RobotShader::getViewMatrixUniform()
{
    return m_viewMatrixUniform;
}

GLuint Shader::RobotShader::getLightPositionUniform()
{
    return m_lightPositionUniform;
}


GLuint Shader::RobotShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}