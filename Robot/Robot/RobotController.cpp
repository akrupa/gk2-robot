//
//  RobotController.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "RobotController.h"
#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

void inverse_kinematics(vec3 pos, vec3 normal, float &a1, float &a2, float &a3, float &a4, float &a5);

Controller::RobotController::RobotController()
{
    m_arms = std::vector<Model::Model *>();
    
    for (int i = 1; i <= 6; i++) {
        Mesh *mesh = new Mesh("mesh" + std::to_string(i) + ".txt");
        
        Model::Model *arm = new Model::Model(mesh);
        arm->color = vec4(0.8f, 0.8f, 0.8f, 1.0);
        m_arms.push_back(arm);
    }
    
}


std::vector<Model::Model *>& Controller::RobotController::getArms()
{
    return m_arms;
}


void Controller::RobotController::setPlanePosition(glm::vec3 position)
{
    m_planePosition = position;
}

void Controller::RobotController::setPlaneNormal(glm::vec3 normal)
{
    m_planeNormal = normal;
}

glm::vec3 Controller::RobotController::getTipPosition()
{
    return m_tipPosition;
}


void Controller::RobotController::simulate(float dt)
{
    const float Radius = 0.5f;
    
    m_alpha += dt;
    
    vec3 normal = glm::normalize(m_planeNormal);

    
    float cosTilt = dot(normal, vec3(0.0f, -1.0f, 0.0f));
    float sinTilt = sqrt(1.0f - cosTilt * cosTilt);
    
    vec3 position = vec3(Radius * cos(m_alpha) * cosTilt,
                         Radius * cos(m_alpha) * sinTilt,
                         Radius * sin(m_alpha));

    position += m_planePosition;
    position.y += 0.5f;
    m_tipPosition = position;
    
    float a1, a2, a3, a4, a5;

    inverse_kinematics(position, normal, a1, a2, a3, a4, a5);
    mat4 cumulative = mat4();
    
    cumulative = cumulative * glm::rotate(mat4(), a1, vec3(0.0f, 1.0f, 0.0f));

    m_arms[1]->setTransformMatrix(cumulative);
    
    cumulative = cumulative * glm::translate(mat4(), vec3(0.0f,  0.27f, -0.26f));
    cumulative = cumulative * glm::rotate(mat4(), a2, vec3(0.0f, 0.0f, -1.0f));
    cumulative = cumulative * glm::translate(mat4(), vec3(0.0f, -0.27f,  0.26f));
    
    m_arms[2]->setTransformMatrix(cumulative);
    
    cumulative = cumulative * glm::translate(mat4(), vec3(-0.91f,  0.27f, -0.26f));
    cumulative = cumulative * glm::rotate(mat4(), a3, vec3(0.0f, 0.0f, -1.0f));
    cumulative = cumulative * glm::translate(mat4(), vec3( 0.91f, -0.27f,  0.26f));
    
    m_arms[3]->setTransformMatrix(cumulative);
    
    
    cumulative = cumulative * glm::translate(mat4(), vec3(-2.05f,  0.27f, -0.26f));
    cumulative = cumulative * glm::rotate(mat4(), a4, vec3(-1.0f, 0.0f, 0.0f));
    cumulative = cumulative * glm::translate(mat4(), vec3( 2.05f, -0.27f,  0.26f));
    
    m_arms[4]->setTransformMatrix(cumulative);
    
    
    cumulative = cumulative * glm::translate(mat4(), vec3(-1.72f,  0.27f, -0.26f));
    cumulative = cumulative * glm::rotate(mat4(), a5, vec3(0.0f, 0.0f, -1.0f));
    cumulative = cumulative * glm::translate(mat4(), vec3( 1.72f, -0.27f,  0.26f));
    
    m_arms[5]->setTransformMatrix(cumulative);

}

void inverse_kinematics(vec3 pos, vec3 normal, float &a1, float &a2, float &a3, float &a4, float &a5)
{
    float l1 = .91f, l2 = .81f, l3 = .33f, dy = .27f, dz = .26f;
    
    vec3 pos1 = pos + normal * l3;
    float e = sqrtf(pos1.z*pos1.z+pos1.x*pos1.x-dz*dz); a1 = atan2(pos1.z, -pos1.x)+atan2(dz, e);
    vec3 pos2(e, pos1.y-dy, .0f);
    a3 = -acosf(min(1.0f,(pos2.x*pos2.x+pos2.y*pos2.y-l1*l1-l2*l2) /(2.0f*l1*l2)));
    float k = l1 + l2 * cosf(a3), l = l2 * sinf(a3);
    a2 = -atan2(pos2.y,sqrtf(pos2.x*pos2.x+pos2.z*pos2.z))-atan2(l,k);
    vec3 normal1;

    normal1 = vec3(glm::rotate(mat4(), -a1, vec3(0.0f, 1.0f, 0.0f)) * vec4(normal.x, normal.y, normal.z, .0f));
    normal1 = vec3(glm::rotate(mat4(), -(a2+a3), vec3(0.0f, 0.0f, 1.0f)) * vec4(normal1.x, normal1.y, normal1.z, .0f));
    a5 = acosf(normal1.x);
    a4 = atan2(normal1.z, normal1.y);
}