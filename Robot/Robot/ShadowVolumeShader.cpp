//
//  ShadowVolumeShader.cpp
//  Robot
//
//  Created by Adrian Krupa on 06.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "ShadowVolumeShader.h"


std::string Shader::ShadowVolumeShader::getShaderName()
{
    return "ShadowVolumeShader";
}

void Shader::ShadowVolumeShader::bindAttributeLocations()
{
    
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    //glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    
}

void Shader::ShadowVolumeShader::fetchUniformLocations()
{
    
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_viewMatrixUniform = glGetUniformLocation(m_program, "viewMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    
    m_lightPositionUniform = glGetUniformLocation(m_program, "gLightPos");
    m_viewProjectionGeometryMatrixUniform = glGetUniformLocation(m_program, "gVP");

}

GLuint Shader::ShadowVolumeShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::ShadowVolumeShader::getViewMatrixUniform()
{
    return m_viewMatrixUniform;
}

GLuint Shader::ShadowVolumeShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}

GLuint Shader::ShadowVolumeShader::getLightPositionUniform()
{
    return m_lightPositionUniform;
}

GLuint Shader::ShadowVolumeShader::getViewProjectionGeometryMatrixUniform()
{
    return m_viewProjectionGeometryMatrixUniform;
}