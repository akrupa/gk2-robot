//
//  ParticleSystem.h
//  Robot
//
//  Created by Adrian Krupa on 02.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__ParticleSystem__
#define __Robot__ParticleSystem__

#include "glm/gtc/matrix_transform.hpp"
#include "Particle.h"
#include "ParticleShader.h"
#include "CameraController.h"
#include <OpenGL/gl3.h>
#include <vector>

using namespace glm;

namespace ParticleSystem {
    
    const int MAX_PARTICLES = 20000000;
    
    const float MAX_PARTICLES_PER_FRAME = 150;
    
    const float MIN_PARTICLE_LIFETIME = 0.1;
    const float MAX_PARTICLE_LIFETIME = 0.5;
    
    const int MIN_PARTICLE_VELOCITY = 1.0;
    const int MAX_PARTICLE_VELOCITY = 2.0;
    
    class ParticleSystem {
        
        vec3 m_planeNormal;
    public:
        ParticleSystem();
        ~ParticleSystem();
        
        
        void setup(vec3 planeNormal);
        void update(vec3 particleSystemPosition, float dt);
        void drawWithCameraController(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
        
        GLuint tex;
        
        Shader::ParticleShader *particleShader;
        size_t bytes_allocated;
        GLuint vbo;
        GLuint vao;                                 
        std::vector<Particle> particles;
        
    private:
        float RandomFloat(float min, float max);
    };
}



#endif /* defined(__Robot__ParticleSystem__) */
