//
//  CameraController.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__CameraController__
#define __Robot__CameraController__

#include <iostream>

#include "glm/glm.hpp"

namespace Controller {
    class CameraController {

        glm::vec3 m_position = glm::vec3(0.0f, 0.0f, -2.0f);
        float m_longitude;
        float m_latitude;
        
    public:
        void mouseDrag(int x, int y);
        void keyboardMove(float x, float y, float z);
        
        glm::vec3 getPosition();
        glm::mat4 getViewMatrix();
        glm::mat4 getProjectionMatrix();
    };
}

#endif /* defined(__Robot__CameraController__) */
