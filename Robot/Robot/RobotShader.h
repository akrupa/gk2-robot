//
//  RobotShader.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__RobotShader__
#define __Robot__RobotShader__

#include <iostream>
#include "Shader.h"

namespace Shader {
    class RobotShader : public Shader {
        
    private:
        GLuint m_viewProjectionMatrixUniform;
        GLuint m_viewMatrixUniform;
        GLuint m_modelMatrixUniform;
        GLuint m_lightPositionUniform;

        GLuint m_drawColorUniform;
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getViewProjectionMatrixUniform();
        GLuint getViewMatrixUniform();
        GLuint getModelMatrixUniform();
        GLuint getLightPositionUniform();
        GLuint getDrawColorUniform();
    };
    
}

#endif /* defined(__Robot__RobotShader__) */
