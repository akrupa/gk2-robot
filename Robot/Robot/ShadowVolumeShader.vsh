#version 330 core

in vec3 in_Position;
in vec3 in_Normal;

out vec3 WorldPos;

uniform mat4 viewProjectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main()
{
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);
    gl_Position = viewProjectionMatrix * worldPosition;
    WorldPos    = worldPosition.xyz;
}
